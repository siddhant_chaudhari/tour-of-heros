import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 1, name: 'Thor - God of Thunder'},
    { id: 2, name: 'Hulk'},
    { id: 3, name: 'Ironman'},
    { id: 4, name: 'Loki'},
    { id: 5, name: 'Captain America'},
    { id: 6, name: 'Black Widow'},
    { id: 7, name: 'Shaktimaan'},
    { id: 8, name: 'Dr. Strange'},
    { id: 9, name: 'Spiderman'},
    { id: 10, name: 'Ultron'}
];